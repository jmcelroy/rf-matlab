%%%% CRF 4.2.7, section 2 slide 46
%%%% Ctrl-R to comment blocks, Ctrl-T to uncomment

clear;
clc;

%%% RL circuits
%% parallel to series

% using element values
% f = 796*10^6;
% Rp = 20;
% Lp = 10*10^-9;
% Xp = 2*pi*f*Lp;
% 
% % Xp = 10;
% Rs = (Rp * Xp^2)/(Rp^2 + Xp^2);
% Xs = (Rp^2 * Xp)/(Rp^2 + Xp^2);
% Ls = (Xs)/(2*pi*f);

% using Q
% Q = 50;
% Lp = 5;
% Rp = 50;
% Ls = (Q^2 * Lp)/(1+Q^2);
% Rs = (Rp)/(1+Q^2);

%% series to parallel

% using element values
% f = 796*10^6;
% Rs = 50;
% Xs = 25;
% Ls = 10*10^-9;
% Xs = 2*pi*f*Ls;

% Xp = (Rs^2 + Xs^2)/(Xs);
% Rp = (Rs^2 + Xs^2)/(Rs);

% using Q
% Ls = 5;
% Rs = 25;
% Lp = ((1/Q^2)+1)/(Ls);
% Rp = (1+Q^2)*(Rs);

%%% RC circuits
%% parallel to series

%using element values
% f = 1*10^9;
% Rp = 18;
% Cp = 12*10^-12;
% Xp = -1/(2*pi*f*Cp);
% %Xp = 10;
% Rs = (Rp * Xp^2)/(Rp^2 + Xp^2);
% Xs = (Rp^2 * Xp)/(Rp^2 + Xp^2);
% Cs = -1/(2*pi*f*(Rs+Xs));

% using Q
% Q = 50;
% Cp = 5;
% Rp = 50;
% Cs = ((1/Q^2)+1)*Cp;
% Rs = (Rp)/(1+Q^2);

%% series to parallel

% using element values
Rs = 14;
Cs = 11*10^-12;
f = 1*10^9;
Xs = -1/(Cs*2*pi*f);
%Xs = 25;
Xp = (Rs^2 + Xs^2)/(Xs);

Rp = (Rs^2 + Xs^2)/(Rs);
Cp = -1/(2*pi*f*(Xp));

% using Q
% Cs = 5;
% Rs = 25;
% Cp = ((Q^2)/(1+Q^2))/(Cs);
% Rp = (1+Q^2)*(Rs);

